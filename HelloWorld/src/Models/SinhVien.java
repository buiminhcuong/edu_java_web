package Models;
public class SinhVien {
	private String CMND;
	private String hoTen;
	private String diaChi;
	private String moTa;
	
	public SinhVien(String cMND, String hoTen, String diaChi, String moTa) {
		super();
		CMND = cMND;
		this.hoTen = hoTen;
		this.diaChi = diaChi;
		this.moTa = moTa;
	}

	public String getCMND() {
		return CMND;
	}

	public void setCMND(String cMND) {
		CMND = cMND;
	}

	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}
}
