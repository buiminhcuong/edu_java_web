

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.SinhVien;

/**
 * Servlet implementation class SV
 * Updated style
 */
@WebServlet("/sv")
public class SV extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private ArrayList<SinhVien> sinhviens = new ArrayList<SinhVien>();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SV() {
        super();
        sinhviens.add(new SinhVien("1", "Nguyen Van An", "Hanoi", ""));
        sinhviens.add(new SinhVien("2", "Nguyen Van Binh", "Nha Trang", ""));
        sinhviens.add(new SinhVien("3", "Nguyen Thuy Chi", "Da Lat", ""));
        sinhviens.add(new SinhVien("4", "Tran Van Dung", "Dien Bien", ""));
        sinhviens.add(new SinhVien("5", "Phan Hong Giang", "Hai Phong", ""));
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		writer.append("<html>");
		writer.append("	<head>");
		writer.append("		<title>Danh sach sinh vien</title>");
		writer.append("		<style>");
		writer.append("			.tbl-data, .tbl-data td{"
									+"border: solid 1px #aaaaaa;"
									+"border-collapse:collapse"
								+ "}");
		writer.append("			.tbl-data {"
									+ "width: 100%;"
								+ "}");
		writer.append("			.tbl-data td {"
									+ "padding: 10px;"
								+ "}");
		writer.append("			.tbl-data tr:hover {"
									+ "background-color: #dedede;"
								+ "}");
		writer.append("			.tbl-head{"
								+ "background-color: blue;"
								+ "color: white;"
								+ "font-weight: bolder;"
							+ "}");
		writer.append("			.tbl-data tr.tbl-head:hover{"
								+ "background-color: blue;"
							+ "}");
		writer.append("		</style>");
		writer.append("	</head>");
		writer.append("	<body>");
		writer.append("		<table class='tbl-data'>");
		writer.append("			<tr class='tbl-head'>");
		writer.append("				<td>So CMND</td><td>Ho ten</td><td>Dia chi</td><td>No ta</td>");
		writer.append("			</tr>");
		for(SinhVien sv : sinhviens) {
			writer.append("		<tr>");
			writer.append("			<td>" + sv.getCMND() + "</td>"
								 + "<td>" + sv.getHoTen() + "</td>"
								 + "<td>" + sv.getDiaChi() + "</td>"
								 + "<td>" + sv.getMoTa() + "</td>");
			writer.append("		</tr>");
		}
		writer.append("		</table>");
		writer.append("	</body>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
