<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.ArrayList,Models.SinhVien"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Danh sach sinh vien</title>
<link rel="stylesheet" type="text/css" href="common/style.css" />
</head>
<body>
	<%
		ArrayList<Models.SinhVien> sinhviens = new ArrayList<Models.SinhVien>();
		sinhviens.add(new Models.SinhVien("1", "Nguyen Van An", "Hanoi", ""));
		sinhviens.add(new Models.SinhVien("2", "Nguyen Van Binh", "Nha Trang", ""));
		sinhviens.add(new Models.SinhVien("3", "Nguyen Thuy Chi", "Da Lat", ""));
		sinhviens.add(new Models.SinhVien("4", "Tran Van Dung", "Dien Bien", ""));
		sinhviens.add(new Models.SinhVien("5", "Phan Hong Giang", "Hai Phong", ""));
	%>
	<table class='tbl-data'>
		<tr class='tbl-head'>
			<td>CMND</td>
			<td>Ho ten</td>
			<td>Dia chi</td>
			<td>Mo ta</td>
		</tr>
		<%
			for (Models.SinhVien sv : sinhviens) {
		%>
		<tr>
			<td><%=sv.getCMND()%></td>
			<td><%=sv.getHoTen()%></td>
			<td><%=sv.getDiaChi()%></td>
			<td><%=sv.getMoTa()%></td>
		</tr>
		<%
			}
		%>
	</table>

</body>
</html>