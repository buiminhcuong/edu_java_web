package mvc1.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {
	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String printHello(ModelMap model) {
		logger.info("Returning hello view");
		model.addAttribute("message", "Hello Spring MVC Framework!");
		return "student/list";
	}
}
