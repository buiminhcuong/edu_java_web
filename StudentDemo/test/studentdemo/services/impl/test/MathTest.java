package studentdemo.services.impl.test;

import org.junit.Assert;
import org.junit.Test;

import studentdemo.services.impl.MyMath;

public class MathTest {
	@Test
	public void maxTest() {
		Assert.assertEquals(10, MyMath.max(5,10));
		Assert.assertEquals(-5, MyMath.max(-20,-5));
		Assert.assertEquals(1234, MyMath.max(1234,400));
	}
}
