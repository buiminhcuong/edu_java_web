package studentdemo.services.impl.test;

import org.junit.Assert;
import org.junit.Test;

import studentdemo.exceptions.StudentException;
import studentdemo.models.City;
import studentdemo.models.Student;
import studentdemo.services.IDataService;
import studentdemo.services.impl.InMemoryDataService;

public class InMemoryDataServiceTest {
	
	private IDataService dataService = new InMemoryDataService();
	
	@Test
	public void createAndGetCityTest() {
		City city = dataService.createCity("HCM", "Ho Chi Minh");
		City result = dataService.getCityById(city.getId());
		
		Assert.assertTrue(result.getId() > 0);
		Assert.assertEquals("HCM", result.getCode());
		Assert.assertEquals("Ho Chi Minh", result.getName());
	}
	
	@Test(expected = StudentException.class)
	public void createAndDeleteStudentTest() {
		City city = dataService.createCity("HN", "Ha Noi");
		City result = dataService.getCityById(city.getId());
		Assert.assertNotNull(result);
		
		dataService.deleteCity(city.getId());
		
		result = dataService.getCityById(city.getId());
	}
	
	@Test
	public void createStudentTest() {
		City city = dataService.createCity("HN", "Ha noi");
		
		long studentId = 1000;
		String name = "Nguyen Van A";
		long cityId = city.getId();
		String dateOfBirth = "1990";
		boolean gender = true;
		float mathMark = 7.0f;
		float physicalMark = 8.5f;
		float chemistryMark = 8.2f;
		
		Student student = 
				dataService.createStudent(studentId, name, cityId, 
						dateOfBirth, gender, mathMark, physicalMark, chemistryMark);
	}
}
