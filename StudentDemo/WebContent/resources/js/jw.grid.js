(function(Grid, $, undefined) {
	
	//provide additional utilities method for standard jQuery dataTables
	Grid.jwTable = function(dataTable) {
		var table = dataTable;
		
		return {
			getTable: function() {
				return table;
			},
			highlightRow: function(id) {
				table.rows(function(idx, dt, node ) {
		 			if(id == dt.id) {
		 				$(node).addClass("data-row-highlight");
		 				setTimeout(function() {
		 					$(node).removeClass("data-row-highlight");
		 				}, 1000);
		 			}
		 		});
		 	},
		 	
		 	showList: function(ajaxUrl) {
				table.rows().remove();
				return $.get(ajaxUrl)
		    	 .done(function(data) {
		    		for(var i = 0; i < data.length; i++) {
						table.row.add(data[i]);
					}
		    		table.draw(true);
		    	 })
		    	 .fail(Common.ajaxError);
			}
		};
	}
})(window.Grid = window.Grid || {}, jQuery);