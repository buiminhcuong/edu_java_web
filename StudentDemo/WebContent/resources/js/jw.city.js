(function(City, $, undefined) {
	var defaultTable = null;
	var ADD_MODAL_SELECTOR = "#add-city";
	var EDIT_MODAL_SELECTOR = "#edit-city";
	
	$(function() {
		 var addValidator = null;
		 var table = City.initTable("#dataTables-city");
		 
    	 City.showList(table);
    	 
    	 $(document).on("click", "#btn-add-city", function () {
	    	 Common.showModal(ADD_MODAL_SELECTOR, Common.Config.urls.city.add);
 		 });
    	 
    	 $(document).on("click", ".command-edit-city", function () {
	    	 Common.showModal(EDIT_MODAL_SELECTOR, Common.Config.urls.city.edit, {data: {id:$(this).data("id")}});
 		 });
    	 
    	 $(document).on("click", ".command-delete-city", function () {
    		 var id = $(this).data("id");
    		 if(confirm("Are you sure you want to delete " + id)) {
    			 City.del(id, table);
    		 }
    		 
 		 });
    	 
    	 $(document).on("click", "#btn-save-city", function() {
    		 Common.validateForm(".city-add-form", function() {
    			 City.add({
        			 code: $("#cityCode").val(),
        			 name: $("#cityName").val()
        		 }, table);
    		 });
    	 });
    	 
    	 $(document).on("click", "#btn-edit-city", function() {
    		 Common.validateForm(".city-edit-form", function() {
    			 City.edit({
    				 id: $(".city-edit-form #cityId").val(),
        			 code: $(".city-edit-form #cityCode").val(),
        			 name: $(".city-edit-form #cityName").val()
        		 }, table);
    		 });
    	 });
    	 
     });
	
	City.add = function(data, table) {
		$.post(Common.Config.urls.city.add, data)
		 .done(function(response) {
			 updateListAndHideModal(response, ADD_MODAL_SELECTOR, table);
		 })
		 .fail(Common.ajaxError);
	}
	
	City.edit = function(data, table) {
		$.post(Common.Config.urls.city.edit, data).done(function(response) {
			 updateListAndHideModal(response, EDIT_MODAL_SELECTOR, table).done(function() {
			 		 var jwTable = new Grid.jwTable(table);
			    	 jwTable.highlightRow(data.id);
			 	});
		 })
		 .fail(Common.ajaxError);
	}
	
	City.del = function(id, table) {
		$.post(Common.Config.urls.city.del, {id:id}).done(function(response) {
			Common.showNotification(response.responseType, response.message);
			City.showList(table);
		 })
		 .fail(Common.ajaxError);
	}
	
	var updateListAndHideModal = function(response, modalSelector, table) {
		 Common.showNotification(response.responseType, response.message);
		 $(modalSelector).modal("hide");
		 return City.showList(table);
	}
	 
	City.initTable = function(selector) {
		var table = $(selector).DataTable({
    		"columns": [
	            { "data": "code", "title": "Code" },
	            { "data": "name", "title": "Name" },
	            { "data": "id", "title": "Edit", "width": "50px", "orderable": false, 
	            	"render": function ( data, type, full, meta ) {
		                return '<a class=\"command-column command-edit-city\" data-id="'+data+'">Edit</a>';
		            }
	            },
	            { "data": "id", "title": "Delete", "width": "50px", "orderable": false, 
	            	"render": function ( data, type, full, meta ) {
		                return '<a class=\"command-column command-delete-city\" data-id="'+data+'">Delete</a>';
		            }
	            }
        	]
    	 });
		return table;
	};
	
	City.showList = function(table) {
		var jwTable = new Grid.jwTable(table);
		jwTable.showList(Common.Config.urls.city.list);
	}
})(window.City = window.City || {}, jQuery);