(function (Common, $, undefined) {
	Common.Config = {
		NotificationDelay: 5000,
		ControllerBaseUrl: "",
		urls: {}
	};
	
	var modalDefaultOption = {data: {}, forceReload: true};
	
	Common.init = function(controllerBaseUrl) {
		var getFullUrl = function(url) {
			return controllerBaseUrl + url;
		}
		
		Common.Config.ControllerBaseUrl = controllerBaseUrl;
		Common.Config.date = {
				format: "dd/mm/yyyy"
		};
		Common.Config.urls = {
			city: {
				list: getFullUrl("city/list.htm"),
				add: getFullUrl("city/add.htm"),
				edit: getFullUrl("city/edit.htm"),
				del: getFullUrl("city/del.htm")
			},
			student: {
				list: getFullUrl("student/list.htm"),
				add: getFullUrl("student/add.htm"),
				edit: getFullUrl("student/edit.htm"),
				del: getFullUrl("student/del.htm")
			}
		};
		
		console.log(Common.Config);
		
		Common.initJqueryPlugins();
	}
	
	$(document).ajaxError(function(jq, textStatus, errorThrown) {
		Common.ajaxError(jq, textStatus, errorThrown);
	});
	
	Common.initJqueryPlugins = function(){
		$('.input-group.date').datepicker({
		    format: Common.Config.date.format,
		    startDate: "01/01/1980",
		    calendarWeeks: true
		});
	} 
	
	//-------  document ready event
	
	$(function() {	
		
	})
	
	$( document ).ajaxComplete(function( event, xhr, settings ) {
		Common.initJqueryPlugins();
	});
	
	//-------  popup utilities
	
	Common.showModal = function(selector, ajaxUrl, opt) {
		var option = $.extend({}, modalDefaultOption, opt);
		if(option.forceReload) {
			$(selector).remove();
		}
		if($(selector).length > 0){
   			$(selector).modal();
   		}
   		else {
   			$.get(ajaxUrl, option.data).done(function(data) {
    	 		$("body").append(data);
    	 		$(selector).modal();
    	 	})
    	 	 .fail(Common.ajaxError);;
   		}
	}
	
	//-------   global notification
	Common.NotificationType = {
		MESSAGE: "MESSAGE",
		WARNING: "WARNING",
		ERROR: "ERROR"
	}
	
	Common.showNotification = function(type, msg, options) {
		this.opt = options;
		this.opt = $.extend({}, {delay: Common.Config.NotificationDelay}, options);
		var contextualClass = "alert-success";
		if(type == Common.NotificationType.ERROR) {
			contextualClass = "alert-danger";
		}
		else if(type == Common.NotificationType.WARNING) {
			contextualClass = "alert-warning";
		}
		var wrapper = $('<div id="global-notification" class="alert ' + contextualClass + ' alert-dismissible popup-notification" role="alert"></div')
			.appendTo('body');
		wrapper.html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
				'<p>' + msg + '</p>');
		
		setTimeout(function() {
			$("#global-notification").hide("slow").remove();
		}, this.opt.delay);
	};
	
	Common.showError = function(msg, options) {
		Common.showNotification(Common.NotificationType.ERROR, msg, options);
	}
	
	Common.showWarning = function(msg, options) {
		Common.showNotification(Common.NotificationType.WARNING, msg, options);
	}
	
	Common.showMessage = function(msg, options) {
		Common.showNotification(Common.NotificationType.MESSAGE, msg, options);
	}
	//!-------   global notification
	
	//-------  validator utilities
	Common.validateForm = function(formSelector, successFn) {
		var validator = $(formSelector).validator().data("bs.validator");
		if(validator == null) {
			return;
		}
		validator.validate();
		if(!validator.hasErrors() && successFn && (typeof successFn === "function")) {
			successFn();
		 }
	}
	//!------  validator utilities
	
	Common.ajaxError = function (jq, textStatus, errorThrown) {
		 if (textStatus && textStatus.responseText) {
			resJson = $.parseJSON(textStatus.responseText);
			Common.showError(resJson.message);
        }
		 else {
			 Common.showError("Unhandled error occurred");
		 }
     };
})(window.Common = window.Common || {}, jQuery);
