(function(Student, $, undefined) {
	var defaultTable = null;
	var ADD_MODAL_SELECTOR = "#add-student";
	var EDIT_MODAL_SELECTOR = "#edit-student";
	
	$(function() {
		 var addValidator = null;
		 var table = Student.initTable("#dataTables-student");
		 
    	 Student.showList(table);
    	 
    	 $(document).on("click", "#btn-add-student", function () {
	    	 Common.showModal(ADD_MODAL_SELECTOR, Common.Config.urls.student.add);
 		 });
    	 
    	 $(document).on("click", ".command-edit-student", function () {
	    	 Common.showModal(EDIT_MODAL_SELECTOR, Common.Config.urls.student.edit, {data: {id:$(this).data("id")}});
 		 });
    	 
    	 $(document).on("click", ".command-delete-student", function () {
    		 var id = $(this).data("id");
    		 if(confirm("Are you sure you want to delete " + id)) {
    			 Student.del(id, table);
    		 }
    		 
 		 });
    	 
    	 $(document).on("click", "#btn-save-student", function() {
    		 Common.validateForm(".form-add-student", function() {
    			 var student = {
    						studentId: $("#studentId").val(),
    						name: $("#studentName").val(),
    						placeOfBirth: $("#studentCityId").val(),
    						dateOfBirth: $("#studentDoB").val(),
    						gender: $("input[name=studentGender]:checked").val(),
    						mathMark: $("#studentMath").val(),
    						physicalMark: $("#studentPhysical").val(),
    						chemistryMark: $("#studentChemistry").val()
    					};
    			 
    			 Student.add(student, table);
    		 });
    	 });
    	 
    	 $(document).on("click", "#btn-edit-student", function() {
    		 Common.validateForm(".form-edit-student", function() {
    			 Student.edit({
    				 id: $(".form-edit-student #studentId").val(),
        			 name: $(".form-edit-student #studentName").val(),
        		 }, table);
    		 });
    	 });
    	 
     });
	
	Student.add = function(data, table) {
		$.post(Common.Config.urls.student.add, data)
		 .done(function(response) {
			 updateListAndHideModal(response, ADD_MODAL_SELECTOR, table);
		 })
		 .fail(Common.ajaxError);
	}
	
	Student.edit = function(data, table) {
		$.post(Common.Config.urls.student.edit, data).done(function(response) {
			 updateListAndHideModal(response, EDIT_MODAL_SELECTOR, table).done(function() {
			 		 var jwTable = new Grid.jwTable(table);
			    	 jwTable.highlightRow(data.id);
			 	});
		 })
		 .fail(Common.ajaxError);
	}
	
	Student.del = function(id, table) {
		$.post(Common.Config.urls.student.del, {id:id}).done(function(response) {
			Common.showNotification(response.responseType, response.message);
			Student.showList(table);
		 })
		 .fail(Common.ajaxError);
	}
	
	var updateListAndHideModal = function(response, modalSelector, table) {
		 Common.showNotification(response.responseType, response.message);
		 $(modalSelector).modal("hide");
		 return Student.showList(table);
	}
	 
	Student.initTable = function(selector) {
		var table = $(selector).DataTable({
    		"columns": [
	            { "data": "studentId", "title": "Student Id" },
	            { "data": "name", "title": "Name" },
	            { "data": "placeOfBirth.name", "title": "Place of Birth" },
	            { "data": "dateOfBirth", "title": "Date of Birth" },
	            { "data": "gender", "title": "Gender", 
	            	"render": function(data, type, full, meta) {
	            		if(data) {
	            			return "Male";
	            		}
	            		return "Female";
	            	} 
	            },
	            { "data": "mathMark", "title": "Math mark" },
	            { "data": "physicalMark", "title": "Physical mark" },
	            { "data": "chemistryMark", "title": "Chemistry mark" },
	            { "data": "id", "title": "Edit", "width": "50px", "orderable": false, 
	            	"render": function ( data, type, full, meta ) {
		                return '<a class=\"command-column command-edit-student\" data-id="'+data+'">Edit</a>';
		            }
	            },
	            { "data": "id", "title": "Delete", "width": "50px", "orderable": false, 
	            	"render": function ( data, type, full, meta ) {
		                return '<a class=\"command-column command-delete-student\" data-id="'+data+'">Delete</a>';
		            }
	            }
        	]
    	 });
		return table;
	};
	
	Student.showList = function(table) {
		var jwTable = new Grid.jwTable(table);
		jwTable.showList(Common.Config.urls.student.list);
	}
})(window.Student = window.Student || {}, jQuery);