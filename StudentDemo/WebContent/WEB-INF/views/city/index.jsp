<%@ page import="java.util.List,studentdemo.models.City"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:genericpage>
	<jsp:attribute name="header">
      Cities
    </jsp:attribute>
	<jsp:attribute name="footer">
      <p id="copyright">cmb</p>
    </jsp:attribute>
    <jsp:attribute name="script">
    	<script src="<c:url value="/resources/js/jw.city.js" />"></script>
    </jsp:attribute>
	<jsp:body>
    	<div class="panel panel-default">
            <div class="panel-heading">
            	<button type="button" id="btn-add-city" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
					Create new city
				</button>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table
						class="table table-striped table-bordered table-hover"
						id="dataTables-city">
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </jsp:body>
</t:genericpage>