<%@page import="java.util.List,studentdemo.models.City"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:modal>
	<jsp:attribute name="modalid">
      edit-city
    </jsp:attribute>
	<jsp:body>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">Edit city</h4>
		</div>
		
		<div class="modal-body">
			<form class="form-horizontal city-edit-form">
				<input type="hidden" id="cityId" value="${city.id}"/>
			  <div class="form-group">
			    <label for="cityCode" class="col-sm-2 control-label">Code</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="cityCode" placeholder="City code" value="${city.code}" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="cityName" class="col-sm-2 control-label">Name</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="cityName" placeholder="City name"  value="${city.name}" required>
			    </div>
			  </div>
			</form>
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" id="btn-edit-city" class="btn btn-primary save-city">Save changes</button>
		</div>
	</jsp:body>
</t:modal>


