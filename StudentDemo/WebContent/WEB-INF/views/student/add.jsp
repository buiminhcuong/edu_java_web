<%@page import="java.util.List,studentdemo.models.City"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:modal>
	<jsp:attribute name="modalid">
      add-student
    </jsp:attribute>
	<jsp:body>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">Add Student</h4>
		</div>
		
		<div class="modal-body">
			<form class="form-horizontal form-add-student">
			  <div class="form-group">
			    <label for="cityCode" class="col-sm-2 control-label">Id</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="studentId" placeholder="Student Id" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="cityName" class="col-sm-2 control-label">Name</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="studentName" placeholder="Student name" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="studentCityId" class="col-sm-2 control-label">City</label>
			    <div class="col-sm-10">
				    <select class="form-control" id="studentCityId" required>
				    	 <option value="" disabled selected>Select city</option>
					     <c:forEach var="city" items="${cities}">
					      	<option value="${city.getId()}">${city.getName()}</option>
						</c:forEach>
					  </select>
			    </div>
			  </div>
	    	  <div class="form-group">
			  	<label for="studentDoB" class="col-sm-2 control-label">DoB</label>
			    	<div class="col-sm-10">
	    			    <div class="input-group date">
							<input type="text" class="form-control" id="studentDoB" placeholder="Date of birth" required>
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
			    	</div>
			  </div>
			  <div class="form-group">
			    <label for="studentGender" class="col-sm-2 control-label">Gender</label>
			    <div class="col-sm-10">
					<div class="radio">
					  <label><input type="radio" name="studentGender" value="true" required>Male</label>
					  <label><input type="radio" name="studentGender" value="false" required>Female</label>
					</div>
			    </div>
			  </div>
			   <div class="form-group">
			    <label for="studentMath" class="col-sm-2 control-label">Math</label>
			    <div class="col-sm-10">
					<div class="radio">
					  <input type="text" class="form-control" id="studentMath" placeholder="Math mark" pattern="^-?\d*(\.\d+)?$" required>
					</div>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="studentMath" class="col-sm-2 control-label">Physical</label>
			    <div class="col-sm-10">
					<input type="text" class="form-control" id="studentPhysical" placeholder="Physical mark" pattern="^-?\d*(\.\d+)?$" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="studentMath" class="col-sm-2 control-label">Chemistry</label>
			    <div class="col-sm-10">
					<input type="text" class="form-control" id="studentChemistry" placeholder="Chemistry mark" pattern="^-?\d*(\.\d+)?$" required>
			    </div>
			  </div>
			</form>
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" id="btn-save-student"
				class="btn btn-primary btn-save-student">Save changes</button>
		</div>
	</jsp:body>
</t:modal>


