<%@ page import="java.util.List,studentdemo.models.City" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:genericpage>
    <jsp:attribute name="header">
      Cities
    </jsp:attribute>
    <jsp:attribute name="footer">
      <p id="copyright">cmb</p>
    </jsp:attribute>
    <jsp:body>
    	<div class="panel panel-default">
            <div class="panel-heading">
            	<a href="city/add.htm">Create new Student</a>
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-city">
                        <thead>
                            <tr>
					    		<td>Code</td>
					    		<td>Name</td>
					    	</tr>
                        </thead>
                        <tbody>
                            <c:forEach var="city" items="${cities}">
								<tr>
									<td>${city.getCode()}</td>
									<td>${city.getName()}</td>
								</tr>
							</c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </jsp:body>
</t:genericpage>