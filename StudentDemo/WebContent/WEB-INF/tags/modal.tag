<%@tag description="Overall Page template" pageEncoding="UTF-8" import="studentdemo.configuration.Configuration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="modalid" fragment="true" %>
<!-- Modal -->
<div class="modal fade" id="<jsp:invoke fragment="modalid"/>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <jsp:doBody/>
    </div>
  </div>
</div>

