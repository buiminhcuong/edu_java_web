<%@tag description="Overall Page template" pageEncoding="UTF-8" import="studentdemo.configuration.Configuration"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="script" fragment="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Studen Demo Application</title>

    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<c:url value="/resources/css/metisMenu.css" />" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/sb-admin-2.css" />" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<c:url value="/resources/css/font-awesome.css" />" rel="stylesheet" type="text/css">
    
    <!-- DataTable CSS -->
    <link href="<c:url value="/resources/css/datatables.min.css" />" rel="stylesheet">    
    
    <!-- Date picker -->
    <link href="<c:url value="/resources/css/bootstrap-datepicker.css" />" rel="stylesheet">
    
    <!-- Application CSS -->
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery -->
    <script src="<c:url value="/resources/js/jquery.js" />"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<c:url value="/resources/js/metisMenu.js" />"></script>
    
    <script src="<c:url value="/resources/js/bootbox.min.js" />"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<c:url value="/resources/js/sb-admin-2.js" />"></script>
    
    <!-- DataTables -->
    <script src="<c:url value="/resources/js/datatables.min.js" />"></script>
    
    <!-- Date picker -->
    <script src="<c:url value="/resources/js/bootstrap-datepicker.min.js" />"></script>
    
    <!-- Validator -->
    <script src="<c:url value="/resources/js/validator.js" />"></script>
    
    
     <!-- Application JavaScripts -->
    <script src="<c:url value="/resources/js/jw.common.js" />"></script>
    <script src="<c:url value="/resources/js/jw.grid.js" />"></script>
</head>

<body class="jw-web">
    <div id="wrapper">
        <!-- Navigation -->
        <jsp:include page="/WEB-INF/includes/nav.jsp">
        	<jsp:param name="title" value="<%= Configuration.PAGE_TITLE %>" />
        </jsp:include>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
      					<jsp:invoke fragment="header"/>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-12">
                	<jsp:doBody/>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
             <div class="page-footer">
   				<%= Configuration.PAGE_FOOTER %>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <jsp:invoke fragment="script"/>
    <script>
    	Common.init("<c:url value="/" />");
    </script>
</body>

</html>