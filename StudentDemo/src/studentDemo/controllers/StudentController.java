package studentdemo.controllers;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import studentdemo.models.City;
import studentdemo.models.Student;
import studentdemo.responses.ResponseMessage;
import studentdemo.services.IDataService;

import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class StudentController {

	protected final Log logger = LogFactory.getLog(getClass());
	private IDataService dataService;

	@Autowired
	public void setDataService(IDataService dataService) {
		this.dataService = dataService;
	}

	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public ModelAndView index() {
		logger.info("Returning student/index view");

		List<Student> allStudents = dataService.getAllStudents();

		return new ModelAndView("student/index", "students", allStudents);
	}

	@RequestMapping(value = "/student/list", method = RequestMethod.GET)
	public @ResponseBody List<Student> getStudents()
			throws JsonProcessingException {
		List<Student> allCities = dataService.getAllStudents();
		return allCities;
	}

	@ModelAttribute("cities")
	public List<City> getAllCities() {
		return dataService.getAllCities();
	}

	@RequestMapping(value = "/student/add", method = RequestMethod.GET)
	public String addStudent(Model model) {
		logger.info("Returning student/add view");
		return "student/add";
	}

	@RequestMapping(value = "/student/add", method = RequestMethod.POST)
	public @ResponseBody ResponseMessage createStudent(long studentId, String name, long placeOfBirth, String dateOfBirth,
			boolean gender, float mathMark, float physicalMark, float chemistryMark) {
		logger.info("Create new student: " + name);
		
		dataService.createStudent(studentId, name,
				placeOfBirth, dateOfBirth,
				gender, mathMark,
				physicalMark, chemistryMark);
		
		ResponseMessage message = ResponseMessage.Builder.successFormat(
				"Student {0} is created successfully", name);
		return message;
	}

	@RequestMapping(value = "/student/del", method = RequestMethod.POST)
	public @ResponseBody ResponseMessage deleteStudent(long id) {
		logger.info("Delete student with id: " + id);
		dataService.deleteStudent(id);
		return ResponseMessage.Builder
				.success("Student is deleted successfully");
	}
}
