package studentdemo.controllers;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import studentdemo.models.City;
import studentdemo.responses.ResponseMessage;
import studentdemo.services.IDataService;

import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class CityController {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private IDataService dataService;
	
	
	@Autowired
	//@Qualifier(value="dbDataService")
    public void setDataService(IDataService dataService) {
        this.dataService = dataService;
    }

	@RequestMapping(value = "/city", method = RequestMethod.GET)
	public ModelAndView index() {
		logger.info("Returning city/index view");
		
		List<City> allCities = dataService.getAllCities();
		
		return new ModelAndView("city/index", "cities", allCities);
	}
	
	@RequestMapping(value = "/city/list", method = RequestMethod.GET)
	public @ResponseBody List<City> getCities() throws JsonProcessingException {
		List<City> allCities = dataService.getAllCities();
		return allCities;
	}
	
	@RequestMapping(value = "/city/add", method = RequestMethod.GET)
	public String addCity() {
		logger.info("Returning city/add view");
		return "city/add";
	}
	
	@RequestMapping(value = "/city/add", method = RequestMethod.POST)
	public @ResponseBody ResponseMessage createCity(@ModelAttribute("cityForm") City city) {
		logger.info("Create new city: " + city.getName());
		dataService.createCity(city.getCode(), city.getName());
		ResponseMessage message = ResponseMessage.Builder
				.successFormat("City {0} is created successfully", city.getName());
		return message;
	}
	
	@RequestMapping(value = "/city/edit", method = RequestMethod.GET)
	public ModelAndView editCity(long id) {
		logger.info("Returning city/edit view");
		City city = dataService.getCityById(id);
		return new ModelAndView("city/edit", "city", city) ;
	}
	
	@RequestMapping(value = "/city/edit", method = RequestMethod.POST)
	public @ResponseBody ResponseMessage editCity(@ModelAttribute("cityForm") City city) {
		logger.info("Edit city: " + city.getId());
		dataService.editCity(city.getId(), city.getCode(), city.getName());
		return ResponseMessage.Builder
				.successFormat("City is edited successfully", city.getName());
	}
	
	@RequestMapping(value = "/city/del", method = RequestMethod.POST)
	public @ResponseBody ResponseMessage deleteCity(long id) {
		logger.info("Delete city with id: " + id);
		dataService.deleteCity(id);
		return ResponseMessage.Builder.success("City is deleted successfully");
	}
}
