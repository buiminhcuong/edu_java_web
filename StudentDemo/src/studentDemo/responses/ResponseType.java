package studentdemo.responses;

public enum ResponseType {
	MESSAGE(0), WARNING(1), ERROR(2);

	private int val;

	ResponseType(int val) {
		this.val = val;
	}

	public int getValue() {
		return val;
	}
}
