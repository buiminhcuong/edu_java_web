package studentdemo.responses;

import java.text.MessageFormat;

public class ResponseMessage {
	private ResponseType responseType;
	private String message;
	
	public ResponseMessage() {
		
	}
	
	public ResponseMessage(ResponseType responseType, String message) {
		super();
		this.responseType = responseType;
		this.message = message;
	}
	
	public ResponseType getResponseType() {
		return responseType;
	}
	public void setResponseType(ResponseType responseType) {
		this.responseType = responseType;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public static class Builder {
		public static ResponseMessage success(String message) {
			return new ResponseMessage(ResponseType.MESSAGE, message);
		}
		
		public static ResponseMessage successFormat(String message, Object... args) {
			return new ResponseMessage(ResponseType.MESSAGE, MessageFormat.format(message, args));
		}
		
		public static ResponseMessage warning(String message) {
			return new ResponseMessage(ResponseType.WARNING, message);
		}
		
		public static ResponseMessage warningFormat(String message, Object... args) {
			return new ResponseMessage(ResponseType.WARNING, MessageFormat.format(message, args));
		}
		
		public static ResponseMessage error(String message) {
			return new ResponseMessage(ResponseType.ERROR, message);
		}
		
		public static ResponseMessage errorFormat(String message, Object... args) {
			return new ResponseMessage(ResponseType.ERROR, MessageFormat.format(message, args));
		}
	}
}
