package studentdemo.common;

import studentdemo.exceptions.InvalidArgumentException;

public class Guards {
	public static void notNull(Object o, String msg) {
		if(o == null) {
			throw new InvalidArgumentException(msg);
		}
	}
	
	public static void notNullOrEmpty(String s, String msg) {
		if(s == null || "".equals(s)) {
			throw new InvalidArgumentException(msg);
		}
	}
}
