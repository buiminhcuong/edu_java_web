package studentdemo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="student")
public class Student {
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private long studentId;
	private String name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "city_id", nullable = false)
	private City placeOfBirth;
	private String dateOfBirth;
	private boolean gender;
	private float mathMark;
	private float physicalMark;
	private float chemistryMark;
	
	public Student() {}

	public Student(long id, long studentId, String name, City placeOfBirth,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		super();
		this.id = id;
		this.studentId = studentId;
		this.name = name;
		this.placeOfBirth = placeOfBirth;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.mathMark = mathMark;
		this.physicalMark = physicalMark;
		this.chemistryMark = chemistryMark;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public City getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(City placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public float getMathMark() {
		return mathMark;
	}

	public void setMathMark(float mathMark) {
		this.mathMark = mathMark;
	}

	public float getPhysicalMark() {
		return physicalMark;
	}

	public void setPhysicalMark(float physicalMark) {
		this.physicalMark = physicalMark;
	}

	public float getChemistryMark() {
		return chemistryMark;
	}

	public void setChemistryMark(float chemistryMark) {
		this.chemistryMark = chemistryMark;
	}

	public long getId() {
		return id;
	}
}
