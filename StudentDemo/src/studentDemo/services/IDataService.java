package studentdemo.services;

import java.util.List;

import studentdemo.models.City;
import studentdemo.models.Student;

public interface IDataService {
	public List<Student> getAllStudents();

	public Student getStudentById(long id);

	public Student createStudent(long studentId, String name, 
			long cityId, String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark);

	public Student editStudent(long id, String name,
			long cityId, String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark);

	public void deleteStudent(long id);

	public List<City> getAllCities();

	public City getCityById(long id);

	public City createCity(String code, String name);

	public City editCity(long id, String code, String name);

	public void deleteCity(long id);
}
