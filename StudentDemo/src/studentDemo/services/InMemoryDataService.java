package studentdemo.services;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import studentdemo.exceptions.StudentException;
import studentdemo.models.City;
import studentdemo.models.Student;

public class InMemoryDataService implements IDataService {

	private List<Student> studentStore = new ArrayList<Student>();
	private List<City> cityStore = new ArrayList<City>();
	
	@Override
	public List<Student> getAllStudents() {
		return studentStore;
	}

	@Override
	public Student getStudentById(long id) {
		for (Student student : studentStore) {
			if (student.getId() == id) {
				return student;
			}
		}
		throw new StudentException(MessageFormat.format(
				"Student with id {0} does not exist", id));
	}

	@Override
	public Student createStudent(long studentId, String name, long cityId,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		long id = studentStore.size() + 1;
		Student student = new Student(id, studentId, name, getCityById(cityId),
				dateOfBirth, gender, mathMark, physicalMark, chemistryMark);
		studentStore.add(student);
		return student;
	}

	@Override
	public Student editStudent(long id, String name, long cityId,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		Student student = getStudentById(id);
		student.setName(name);
		student.setPlaceOfBirth(getCityById(id));
		student.setMathMark(mathMark);
		student.setPhysicalMark(physicalMark);
		student.setChemistryMark(chemistryMark);
		return student;
	}

	@Override
	public void deleteStudent(long id) {
		Student tobeRemoved = null;
		for (Student student : studentStore) {
			if (student.getId() == id) {
				tobeRemoved = student;
			}
		}
		if (tobeRemoved != null) {
			studentStore.remove(tobeRemoved);
		} else {
			throw new StudentException(MessageFormat.format(
					"Student with id {0} does not exist", id));
		}
	}

	@Override
	public List<City> getAllCities() {
		return cityStore;
	}

	@Override
	public City getCityById(long cityId) {
		for (City city : cityStore) {
			if (city.getId() == cityId) {
				return city;
			}
		}
		throw new StudentException(MessageFormat.format(
				"City with id {0} does not exist", cityId));

	}

	@Override
	public City createCity(String name, String code) {
		long id = cityStore.size() + 1;
		City city = new City(id, name, code);
		cityStore.add(city);
		return city;
	}

	@Override
	public City editCity(long id, String code, String name) {
		City city = getCityById(id);
		city.setCode(code);
		city.setName(name);
		return city;
	}

	@Override
	public void deleteCity(long id) {
		City toBeRemoved = getCityById(id);
		cityStore.remove(toBeRemoved);
	}
}
