package studentdemo.services.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import studentdemo.common.Guards;
import studentdemo.exceptions.BusinessException;
import studentdemo.exceptions.StudentException;
import studentdemo.models.City;
import studentdemo.models.Student;
import studentdemo.services.IDataService;

public class InMemoryDataService implements IDataService {
	private List<Student> studentStore = new ArrayList<Student>();
	private List<City> cityStore = new ArrayList<City>();
	
	public InMemoryDataService() {
		initTestData();
	}
	
	public void initTestData() {
		cityStore.add(new City(1, "HN", "Ha Noi"));
		cityStore.add(new City(2, "HCM", "Ho Chi Minh"));
	}

	@Override
	public List<Student> getAllStudents() {
		return studentStore;
	}

	@Override
	public Student getStudentById(long id) {
		for (Student student : studentStore) {
			if (student.getId() == id) {
				return student;
			}
		}
		throw new StudentException(MessageFormat.format(
				"Studen with id {0} does not exist", id));
	}

	@Override
	public Student createStudent(long studentId, String name, long cityId,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		long id = studentStore.size() + 1;
		City placeOfBirth = getCityById(cityId);
		Student student = new Student(id, studentId, name, placeOfBirth,
				dateOfBirth, gender, mathMark, physicalMark, chemistryMark);
		
		studentStore.add(student);
		
		return student;
	}

	@Override
	public Student editStudent(long id, String name, long cityId,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		
		Student student = getStudentById(id);
		student.setName(name);
		student.setPlaceOfBirth(getCityById(cityId));
		student.setGender(gender);
		student.setMathMark(mathMark);
		student.setPhysicalMark(physicalMark);
		student.setChemistryMark(chemistryMark);
		
		return student;
	}

	@Override
	public void deleteStudent(long id) {
		Student student = getStudentById(id);
		studentStore.remove(student);
	}

	@Override
	public List<City> getAllCities() {
		return cityStore;
	}

	@Override
	public City getCityById(long id) {
		for (City city : cityStore) {
			if (city.getId() == id) {
				return city;
			}
		}
		throw new StudentException(MessageFormat.format(
				"City with id {0} does not exist", id));
	}
	
	public City tryGetCityByCode(String code) {
		for (City city : cityStore) {
			if (city.getCode().equalsIgnoreCase(code)){
				return city;
			}
		}
		return null;
	}

	@Override
	public City createCity(String code, String name) {
		Guards.notNullOrEmpty(code, "code cannot be null  or empty");
		Guards.notNullOrEmpty(name, "name cannot be null or empty");
		
		long id = cityStore.size() + 1;
		City exist = tryGetCityByCode(code);
		if(exist != null) {
			throw new BusinessException("City with code '" + code + "' already exist");
		}
		City city = new City(id, code, name);
		cityStore.add(city);
		return city;
	}

	@Override
	public City editCity(long id, String code, String name) {
		Guards.notNullOrEmpty(code, "code cannot be null  or empty");
		Guards.notNullOrEmpty(name, "name cannot be null or empty");
		
		City city = getCityById(id);
		city.setCode(code);
		city.setName(name);
		return city;
	}

	@Override
	public void deleteCity(long id) {
		City city = getCityById(id);
		cityStore.remove(city);
	}

}
