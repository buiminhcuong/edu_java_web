package studentdemo.services.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import studentdemo.models.City;
import studentdemo.models.Student;
import studentdemo.services.IDataService;

@Repository
@Transactional
public class DbDataService implements IDataService {

	private SessionFactory sessionFactory;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public List<Student> getAllStudents() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Student> students = session.createQuery("from Student").list();
		return students;
	}

	@Override
	public Student getStudentById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student createStudent(long studentId, String name, long cityId,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student editStudent(long id, String name, long cityId,
			String dateOfBirth, boolean gender, float mathMark,
			float physicalMark, float chemistryMark) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteStudent(long id) {

	}

	@Override
	public List<City> getAllCities() {
		Session session = this.sessionFactory.getCurrentSession();
		List<City> cities = session.createQuery("from City").list();
		return cities;
	}

	@Override
	public City getCityById(long id) {
		City city = (City) getSession().get(City.class, id);
		return city;
	}

	@Override
	public City createCity(String code, String name) {
		City city = new City();
		city.setName(name);
		city.setCode(code);
		Session session = this.sessionFactory.getCurrentSession();
		session.save(city);
		return city;
	}

	@Override
	public City editCity(long id, String code, String name) {
		City city = getCityById(id);
		city.setName(name);
		city.setCode(code);
		getSession().save(city);
		return city;
	}

	@Override
	public void deleteCity(long id) {
		City city = getCityById(id);
		getSession().delete(city);
	}

}
