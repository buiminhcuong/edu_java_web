package studentdemo.exceptions;
public class BusinessException extends StudentException {

    public BusinessException(String msg) {
        super(msg);
    }
}