package studentdemo.exceptions;
public class NotFoundException extends StudentException {

    public NotFoundException(String msg) {
        super(msg);
    }
}