package studentdemo.exceptions;

public class InvalidArgumentException extends StudentException {
	public InvalidArgumentException(String msg) {
		super(msg);
	}
}
