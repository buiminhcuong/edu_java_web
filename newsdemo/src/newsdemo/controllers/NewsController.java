package newsdemo.controllers;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Controller
public class NewsController{
	protected final Log logger = LogFactory.getLog(getClass());
	 
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	   public String printHello(ModelMap model) {
		  logger.info("Returning hello view");
	      model.addAttribute("title", "Hello Spring MVC Framework!");
	      return "news/list";
	   }

}